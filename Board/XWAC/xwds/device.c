/**
 * @file
 * @brief 板级描述层：XWOS适配层：XWOS设备栈：设备
 * @author
 * + 隐星魂 (Roy Sun) <xwos@xwos.tech>
 * @copyright
 * + Copyright © 2015 xwos.tech, All Rights Reserved.
 * > Licensed under the Apache License, Version 2.0 (the "License");
 * > you may not use this file except in compliance with the License.
 * > You may obtain a copy of the License at
 * >
 * >         http://www.apache.org/licenses/LICENSE-2.0
 * >
 * > Unless required by applicable law or agreed to in writing, software
 * > distributed under the License is distributed on an "AS IS" BASIS,
 * > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * > See the License for the specific language governing permissions and
 * > limitations under the License.
 */

#include "Board/std.h"
#include "XWAC/xwds/device.h"

struct xwds stm32xwds;

void stm32xwds_init(void)
{
        xwds_init(&stm32xwds);
}

void stm32xwds_fini(void)
{
}

xwer_t stm32xwds_soc_init(void)
{
        xwer_t rc;

        xwds_soc_construct(&stm32xwds_soc);
        rc = xwds_device_probe(&stm32xwds,
                               xwds_cast(struct xwds_device *, &stm32xwds_soc),
                               NULL);
        if (__xwcc_unlikely(rc < 0)) {
                goto err_dev_probe;
        }
        rc = xwds_device_start(xwds_cast(struct xwds_device *, &stm32xwds_soc));
        if (__xwcc_unlikely(rc < 0)) {
                goto err_dev_start;
        }
        return XWOK;

err_dev_start:
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_soc));
err_dev_probe:
        xwds_soc_destruct(&stm32xwds_soc);
        return rc;
}

xwer_t stm32xwds_soc_fini(void)
{
        xwds_device_stop(xwds_cast(struct xwds_device *, &stm32xwds_soc));
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_soc));
        xwds_soc_destruct(&stm32xwds_soc);
        return XWOK;
}

xwer_t stm32xwds_uart_init(void)
{
        xwer_t rc;

        xwds_uartc_construct(&stm32xwds_usart1);
        rc = xwds_device_probe(&stm32xwds,
                               xwds_cast(struct xwds_device *, &stm32xwds_usart1),
                               NULL);
        if (__xwcc_unlikely(rc < 0)) {
                goto err_usart1_probe;
        }
        rc = xwds_device_start(xwds_cast(struct xwds_device *, &stm32xwds_usart1));
        if (__xwcc_unlikely(rc < 0)) {
                goto err_usart1_start;
        }

        xwds_uartc_construct(&stm32xwds_uart5);
        rc = xwds_device_probe(&stm32xwds,
                               xwds_cast(struct xwds_device *, &stm32xwds_uart5),
                               NULL);
        if (__xwcc_unlikely(rc < 0)) {
                goto err_uart5_probe;
        }
        rc = xwds_device_start(xwds_cast(struct xwds_device *, &stm32xwds_uart5));
        if (__xwcc_unlikely(rc < 0)) {
                goto err_uart5_start;
        }
        return XWOK;

err_uart5_start:
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_uart5));
err_uart5_probe:
        xwds_uartc_destruct(&stm32xwds_uart5);

        xwds_device_stop(xwds_cast(struct xwds_device *, &stm32xwds_usart1));
err_usart1_start:
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_usart1));
err_usart1_probe:
        xwds_uartc_destruct(&stm32xwds_usart1);
        return rc;
}

xwer_t stm32xwds_uart_fini(void)
{
        xwds_device_stop(xwds_cast(struct xwds_device *, &stm32xwds_uart5));
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_uart5));
        xwds_uartc_destruct(&stm32xwds_uart5);

        xwds_device_stop(xwds_cast(struct xwds_device *, &stm32xwds_usart1));
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_usart1));
        xwds_uartc_destruct(&stm32xwds_usart1);
        return XWOK;
}

xwer_t stm32xwds_i2c_init(void)
{
        xwer_t rc;

        /* I2C1 */
        xwds_i2cm_construct(&stm32xwds_i2c1m);
        rc = xwds_device_probe(&stm32xwds,
                               xwds_cast(struct xwds_device *, &stm32xwds_i2c1m),
                               NULL);
        if (rc < 0) {
                goto err_i2c1_probe;
        }
        rc = xwds_device_start(xwds_cast(struct xwds_device *, &stm32xwds_i2c1m));
        if (rc < 0) {
                goto err_i2c1_start;
        }

        return XWOK;

err_i2c1_start:
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_i2c1m));
err_i2c1_probe:
        xwds_i2cm_destruct(&stm32xwds_i2c1m);
        return rc;
}

xwer_t stm32xwds_i2c_fini(void)
{
        /* I2C1 */
        xwds_device_stop(xwds_cast(struct xwds_device *, &stm32xwds_i2c1m));
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_i2c1m));
        xwds_i2cm_destruct(&stm32xwds_i2c1m);
        return XWOK;
}

xwer_t stm32xwds_eeprom_init(void)
{
        xwer_t rc;

        xwds_eeprom_construct(&stm32xwds_eeprom256);
        rc = xwds_device_probe(&stm32xwds,
                               xwds_cast(struct xwds_device *, &stm32xwds_eeprom256),
                               NULL);
        if (rc < 0) {
                goto err_dev_probe;
        }
        rc = xwds_device_start(xwds_cast(struct xwds_device *, &stm32xwds_eeprom256));
        if (rc < 0) {
                goto err_dev_start;
        }

        return XWOK;

err_dev_start:
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_eeprom256));
err_dev_probe:
        xwds_eeprom_destruct(&stm32xwds_eeprom256);
        return rc;
}

xwer_t stm32xwds_eeprom_fini(void)
{
        xwds_device_stop(xwds_cast(struct xwds_device *, &stm32xwds_eeprom256));
        xwds_device_remove(xwds_cast(struct xwds_device *, &stm32xwds_eeprom256));
        xwds_eeprom_destruct(&stm32xwds_eeprom256);
        return XWOK;
}
