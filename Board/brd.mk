#! /bin/make -f
# @file
# @brief 板级描述层的编译规则
# @author
# + 隐星魂 (Roy Sun) <xwos@xwos.tech>
# @copyright
# + Copyright © 2015 xwos.tech, All Rights Reserved.
# > Licensed under the Apache License, Version 2.0 (the "License");
# > you may not use this file except in compliance with the License.
# > You may obtain a copy of the License at
# >
# >         http://www.apache.org/licenses/LICENSE-2.0
# >
# > Unless required by applicable law or agreed to in writing, software
# > distributed under the License is distributed on an "AS IS" BASIS,
# > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# > See the License for the specific language governing permissions and
# > limitations under the License.
#

BRD_INCDIRS := ..
BRD_INCDIRS += ../STM32Cube/Core/Inc
BRD_INCDIRS += ../STM32Cube/Drivers/STM32H7xx_HAL_Driver/Inc
BRD_INCDIRS += ../STM32Cube/Drivers/STM32H7xx_HAL_Driver/Inc/Legacy
BRD_INCDIRS += ../STM32Cube/Drivers/CMSIS/Device/ST/STM32H7xx/Include
BRD_INCDIRS += ../STM32Cube/Drivers/CMSIS/Include

BRD_PREDEF := -DUSE_FULL_LL_DRIVER
BRD_PREDEF += -DUSE_HAL_DRIVER
BRD_PREDEF += -DSTM32H743xx

BRD_AFLAGS := $(BRD_PREDEF)
BRD_CFLAGS := $(BRD_PREDEF)
BRD_CXXFLAGS := $(BRD_PREDEF)
BRD_LDFLAGS := $(BRD_PREDEF)
BRD_CFLAGS += -Wno-unused-parameter -Wno-sign-conversion
BRD_CXXFLAGS += -Wno-unused-parameter -Wno-sign-conversion
BRD_LDFLAGS_gcc := -Wl,--no-warn-rwx-segment

BRD_ASRCS :=
BRD_CSRCS :=
BRD_CXXSRCS :=

BRD_EOBJS :=

BRD_CSRCS += init.c
BRD_CSRCS += version.c
BRD_CSRCS += firmware.c
BRD_CSRCS += axisram.c

BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/xwos)
BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/xwlib)
BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/xwds)
BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/lua)
BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/fatfs)
ifeq ($(XWCFG_LIBC),newlib)
  BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/newlib)
endif
ifeq ($(XWCFG_LIBC),picolibc)
  BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/picolibc)
endif
BRD_CSRCS += $(call BrdWildcard,*.c,XWAC/xwrust)
