
# 介绍

本项目包括一个示例工程。此工程是在野火Stm32H743 Pro开发板上移植XWOS的示例工程。

+ 硬件主页：<http://doc.embedfire.com/products/link/zh/latest/stm32/ebf_stm32h743_pro/download/stm32h743_pro.html>
+ IDE：STM32CubeIDE
+ MCU: STM32H743

![EmbedFireSTM32H743.jpg](Doc/EmbedFireSTM32H743.jpg)


## XWOS

玄武操作系统（XWOS®）开发于2015年，是一款运行在嵌入式微控制器上的通用实时操作系统，
主要面向可靠、实时、安全的应用领域，包括汽车、物联网、工控等。

+ <http://xwos.tech/>
+ <http://xwos.org/>
+ <https://xwos.gitee.io/>
+ <https://xwos.github.io/>


# 开发指南

## 下载源码

```shell
git clone --recursive https://gitee.com/xwos/EmbedFireSTM32H743XWOS.git
```


## 搭建开发环境

参考 <http://xwos.tech/docs/UserManual/Board/STM32/Env/>


## 导入工程到STM32CubeIDE

工程需放在STM32CubeIDE的工作目录内（即STM32CubeIDE启动时指定的目录）。

+ 菜单： **File --> Import... --> General --> Projects from Folder or Archive**


## STM32CubeMX配置工程

MCU的驱动是使用 **STM32CubeMX** 进行配置的，并未直接在 **STM32CubeIDE** 内配置。
**STM32CubeMX** 配置文件的路径 `STM32Cube/EmbedFireSTM32H743XWOS.ioc` 。

用户修改配置时，需要注意中断优先级的要求：

```
切换上下文的中断为系统中最低优先级中断
切换上下文的中断 <= 滴答定时器的中断 <= 调度器服务中断
```

+ NVIC设置
  + 设置 **3** 个抢占优先级位和 **1** 个子优先级位；
  + SVC中断设置成 **最高** 优先级，即 **Preemption Priority** 为 **0** ；
  + PendSV中断设置成 **最低** 优先级，即 **Preemption Priority** 为 **7** ；
  + Systick中断设置成 **最低** 优先级，即 **Preemption Priority** 为 **7** ；
  + 系统Fault的优先级设置为 **0** ；
  + 其他中断的优先级只可在 **1~6** 之间。


## 编译

有两种方法可以编译，两种方法结果产生一样：

+ 打开终端，并将当前路径切换到工程的 `Board` 目录，执行 `make` 命令。
+ 在IDE中点击 **编译** 按钮。


### 已知问题

+ 平台：Windows 11
+ 问题现象：STM32CubeIDE中编译时，提示错误 `make (e=3): 系统找不到指定的路径。`
+ 问题原因：STM32CubeIDE自带的 `make` 命令版本不兼容。
+ 解决方法：使用 **XWTC** 中提供的 `make` 命令，设置方法如下：

![img](https://gitee.com/xwos/WeActMiniStm32H750XWOS/raw/main/Doc/EclipseBuilderSettings.png "EclipseBuilderSettings")


## 调试

+ 菜单： **Run --> Debug Configurations...** ：
  + **EmbedFireSTM32H743XWOS-Openocd-DAPLink.launch** ： **gdb** + **Openocd** + **DAPLink**
  + **EmbedFireSTM32H743XWOS-JLink.launch** ： **gdb** + **JLink**
  + **EmbedFireSTM32H743XWOS-STLink.launch** ： **gdb** + **STLink**

### 调试步骤

+ 准备一张SD卡，格式化为FAT32文件系统；
+ 将文件夹 `XWOS/xwam/example/lua/` 内的lua脚本拷贝到SD卡内；
+ 将SD卡插入开发板；
+ 使用USB线连接电路板上的USB-232接口与PC；
+ 打开终端工具（windows可选putty、SecretCRT，Linux可选gtkterm、picocom、minicom），但先不要在终端工具中打开串口，否则MCU会进入下载模式；
+ 上电，并在STM32CubeIDE中启动调试，等待MCU运行到初始断点；
+ 在终端工具中打开串口，参数：1M波特率/8位数据/1位停止位/无奇偶校验；
+ 开始单步调试或连续运行调试。

### 调试技巧

ARMv7-M7的CPU带有Cache，常常会调试带来困扰。
将下面的宏开关注释掉或定义改为 `0` 后可关闭Cache：

``` C
/* Board/cfg/board.h */

#define BRDCFG_DCACHE   1
#define BRDCFG_ICACHE   1
```


## 运行过程

+ 程序入口： `App/main.c`
+ 启动流程：
  + `xwos_main()` 创建一个主线程 `main_task()` ；
  + `main_task()` 依次调用各个 [**玄武模块**](http://xwos.tech/docs/user-manual/build-system/#玄武模块) 的 `xxx_start()` 函数完成对各个模块加载；
  + 开始并行执行各个模块中创建的线程。

![img](Doc/xwlua-repl.png "XWLUA交互式解释器")


# 客制化

## 工程改名

1. 可通过命令 `find . -type f -name "*EmbedFireSTM32H743XWOS*"` 搜索所有文件名包含 `EmbedFireSTM32H743XWOS` 的文件，将其改为 **新工程名** 。
2. 可通过命令 `find . -path "./.git" -prune -o -type f -exec grep -i -H "EmbedFireSTM32H743XWOS" {} \;` 搜索所有内容包含 `EmbedFireSTM32H743XWOS` 的文件，
   然后依次打开，将其全部替换为 **新工程名** 。


## 连接到外部静态库

可在文件 `Board/lib.mk` 中定义外部静态库：

+ `EINCDIRS` ：外部静态库头文件列表
  + `EINCDIRS_gcc` ：只对gcc编译器生效的外部静态库头文件列表
  + `EINCDIRS_llvm` ：只对clang编译器生效的外部静态库头文件列表
+ `ELIBS` ：外部静态库列表
  + `ELIBS_gcc` ：只对gcc编译器生效的外部静态库列表
  + `ELIBS_llvm` ：只对clang编译器生效的外部静态库列表


# 源码说明

## 源码结构

![XWOS架构图](http://xwos.tech/docs/TechRefManual/Architecture/xwos-arch_hud68e8e8565694c7eb1f134c79006dda4_264347_903x688_fill_catmullrom_smart1_3.png)

+ 本工程的根目录：也是 `$(XWOS_OEM_DIR)` 目录，App, RustApp, STM32Cube, Test目录都是以 **OEM模块** 集成到本工程中的。
+ XWOS：XWOS的源码，也是编译时 **头文件** 的搜索根目录。
+ Board： **电路板目录** ，也是编译开始的目录，在XWOS的编译系统中，用 `$(XWOS_BRD_DIR)` 表示此目录。
+ STM32Cube：STM32CubeMX配置的驱动库工程
+ App：用户的C/C++应用程序模块
+ RustApp：用户的Rust应用程序模块
+ Test：测试程序
+ Tools：脚本与工具
+ DebugConfiguration：调试配置文件
+ Doc：文档
+ CHANGELOG.md：变更日志
+ LICENSE.txt：开源协议
+ README.md：使用说明（本文件）


## BSP

+ STM32Cube模块： `STM32Cube`
  + STM32CubeMX配置文件： `STM32Cube/EmbedFireSTM32H743XWOS.ioc`
+ 中断向量表： `STM32Cube/Core/Src/ivt.c`
  + 自动生成的脚本： `Tools/el/stm32ivt.el`
+ AXI-SRAM
  + 映射地址： **0x24000000**
  + 大小： **512KiB**
  + 内存管理算法：伙伴算法 `XWOS/xwos/mm/bma.c`
    + 头文件
      + `Board/board/axisram.h`
+ DTCM
  + 映射地址： **0x20000000**
  + 大小： **128KiB**
  + 内存管理算法：伙伴算法 `XWOS/xwos/mm/bma.c`
    + 头文件
      + `Board/board/dtcm.h`
+ SDRAM
  + 映射地址： **0xD0000000**
  + 大小： **64MiB**
  + 内存管理算法：内存池 `XWOS/xwos/mm/mempool`
    + 头文件
      + `Board/sdram.h`
    + 覆盖libc的动态内存管理的函数，使用C的 `malloc()/free()` 或C++的 `new/delete` 关键字时，将从SDRAM申请内存。
      + 相关实现，参考：
        + `XWOS/xwmd/libc/newlibac/mem.c`
        + `Board/XWAC/newlib/mem.c`
        + `XWOS/xwmd/libc/picolibcac/mem.c`
        + `Board/XWAC/picolibc/mem.c`
+ QSPI Flash
  + 映射地址： **0x90000000**
  + 大小： **32MiB**
  + 驱动： `STM32Cube/Core/Src/quadspi.c`
+ 终端UART
  + 设备：USART1
  + 参数：1M波特率/8位数据/1位停止位/无奇偶校验
  + 驱动（由上往下分为三层）
    + 设备栈提供统一的接口框架： `XWOS/xwmd/ds/uart`
    + 适配设备栈接口的驱动： `Board/XWAC/xwds/uart.c`
    + STM32Cube驱动： `STM32Cube/Core/Src/usart.c`
      + 注意：官方驱动库中的驱动存在BUG，XWOS所提供的代码已修复。
+ SOC间进行通讯的UART
  + 设备：UART5
  + 参数：1M波特率/8位数据/1位停止位/无奇偶校验
  + 驱动（由上往下分为三层）
    + 设备栈提供统一的接口框架： `XWOS/xwmd/ds/uart`
    + 适配设备栈接口的驱动： `Board/XWAC/xwds/uart.c`
    + STM32Cube驱动： `STM32Cube/Core/Src/usart.c`
      + 注意：官方驱动库中的驱动存在BUG，XWOS所提供的代码已修复。
+ SD卡
  + 文件系统： `XWOS/xwem/fs/fatfs`
    + 文件系统接口： `Board/XWAC/fatfs`
    + STM32Cube驱动： `STM32Cube/Core/Src/sdmmc.c`


## Lua

+ 版本： **5.4**
+ LUA虚拟机： `XWOS/xwem/vm/lua/src`
+ XWLUA适配层： `XWOS/xwem/vm/lua/src`
+ LUA虚拟机运行时的内存池： `Board/XWAC/lua`
+ 已注册的驱动库： `LuaMod/ds.c`
  + SOC: `stm32`
  + UART: `uart1` , `uart5`


## FatFS

+ 版本： **0.15**
+ 文件系统路径： `"sd:/"`
+ 源码： `XWOS/xwem/fs/fatfs`
  + 文件系统接口： `Board/XWAC/fatfs`
  + STM32Cube驱动： `STM32Cube/Core/Src/sdmmc.c`


## libc

XWOS支持 **gcc+newlib** 或 **llvm+picolibc** 。

### newlib

+ 驱动层适配代码： `Board/XWAC/newlib`
  + 标准输入、输出/错误：重定向至 **USART1**
  + 文件系统：位于SD卡内，从路径 `sd:/` 开始
  + 动态内存管理：由 **SDRAM** 的内存池提供

### picolibc

+ 驱动层适配代码： `Board/XWAC/picolibc`
  + 标准输入、输出/错误：重定向至 **USART1**
  + 文件系统：位于SD卡内，从路径 `sd:/` 开始
  + 动态内存管理：由 **SDRAM** 的内存池提供


## 电源管理

+ 驱动： `Board/XWAC/xwds/pm.c`


## Rust

+ 入口： `Rustapp` : `xwrust_main()`
  + 被主线程 `main_task()` 调用。
+ 示例：可在 `xwrust_main()` 中调用示例，快速上手XWRUST。
  + 动态线程： `XWOS/xwam/xwrust-example/xwrust_example_dthd`
  + 静态线程： `XWOS/xwam/xwrust-example/xwrust_example_sthd`
  + 软件定时器： `XWOS/xwam/xwrust-example/xwrust_example_swt`
  + 锁
    + 互斥锁： `XWOS/xwam/xwrust-example/xwrust_example_mutex`
    + 自旋锁： `XWOS/xwam/xwrust-example/xwrust_example_spinlock`
    + 顺序锁： `XWOS/xwam/xwrust-example/xwrust_example_seqlock`
  + 同步
    + 信号量： `XWOS/xwam/xwrust-example/xwrust_example_sem`
    + 条件量： `XWOS/xwam/xwrust-example/xwrust_example_cond`
    + 事件标志： `XWOS/xwam/xwrust-example/xwrust_example_flg`
    + 线程栅栏： `XWOS/xwam/xwrust-example/xwrust_example_br`
    + 信号选择器： `XWOS/xwam/xwrust-example/xwrust_example_sel`
  + 循环队列： `XWOS/xwam/xwrust-example/xwrust_example_xwcq`
  + 消息队列： `XWOS/xwam/xwrust-example/xwrust_example_xwmq`


# 示例

XWOS中有很多示例，用户可以在 `main_task()` 主线程中调用示例的 `xxx_start()` 进行实验，
可以快速上手XWOS。

+ 线程
  + 创建线程： `XWOS/xwam/example/thd/new`
  + 线程睡眠： `XWOS/xwam/example/thd/sleep`
  + 线程退出： `XWOS/xwam/example/thd/exit`
+ 定时器： `XWOS/xwam/example/swt`
+ 同步
  + 信号量： `XWOS/xwam/example/sync/sme`
  + 条件量： `XWOS/xwam/example/sync/cond`
  + 事件标志： `XWOS/xwam/example/sync/flg`
  + 线程栅栏： `XWOS/xwam/example/sync/br`
  + 信号选择器： `XWOS/xwam/example/sync/sel`
+ 锁
  + 中断管理： `XWOS/xwam/example/lock/cpuirq`
  + 自旋锁： `XWOS/xwam/example/lock/spinlock`
  + 顺序锁： `XWOS/xwam/example/lock/seqlock`
  + 互斥锁： `XWOS/xwam/example/lock/mtx`
+ 内存管理
  + 简单内存分配器： `XWOS/xwam/example/mm/sma`
  + 内存切片分配器： `XWOS/xwam/example/mm/memslice`
  + 伙伴算法分配器： `XWOS/xwam/example/mm/bma`
+ 玄武C库
  + CRC： `XWOS/xwam/example/xwlib/crc`
+ C++： `XWOS/xwam/example/cxx`

实验时，最好一次只启动一个示例，避免日志冲突。
实验时，也可将Lua虚拟机关闭。方法是将配置文件 `Board/cfg/xwem.h` 中的配置 `XWEMCFG_vm_lua` 改为 `0` 。
