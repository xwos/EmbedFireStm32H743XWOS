/**
 * @file
 * @brief 测试程序：eeprom
 * @author
 * + 隐星魂 (Roy Sun) <xwos@xwos.tech>
 * @copyright
 * + Copyright © 2015 xwos.tech, All Rights Reserved.
 * > Licensed under the Apache License, Version 2.0 (the "License");
 * > you may not use this file except in compliance with the License.
 * > You may obtain a copy of the License at
 * >
 * >         http://www.apache.org/licenses/LICENSE-2.0
 * >
 * > Unless required by applicable law or agreed to in writing, software
 * > distributed under the License is distributed on an "AS IS" BASIS,
 * > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * > See the License for the specific language governing permissions and
 * > limitations under the License.
 */

#include "Board/std.h"
#include <xwos/lib/xwlog.h>
#include <xwos/osal/time.h>
#include <xwcd/peripheral/i2c/eeprom/driver.h>
#include "Board/XWAC/xwds/device.h"

#define LOGTAG "TST|EEPROM"

void board_eeprom_test_write(void)
{
        xwu8_t wrbuf[9] = "XWOS-3.0";
        xwsz_t wrsz;
        xwer_t rc;

        wrsz = sizeof(wrbuf) - 1;
        rc = xwds_eeprom_pgwrite(&stm32xwds_eeprom256,
                                 wrbuf, &wrsz, 0,
                                 xwtm_ft(xwtm_s(1)));
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Failed to write ... <%ld>\n", rc);
        } else {
                xwlogf(INFO, LOGTAG, "Write:%s\n", wrbuf);
        }
}

void board_eeprom_test_read(void)
{
        xwu8_t rdbuf[9];
        xwsz_t rdsz;
        xwer_t rc;

        rdsz = sizeof(rdbuf) - 1;
        rc = xwds_eeprom_pgread(&stm32xwds_eeprom256,
                                rdbuf, &rdsz, 0,
                                xwtm_ft(xwtm_s(1)));
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Failed to read ... <%ld>\n", rc);
        } else {
                rdbuf[8] = 0;
                xwlogf(INFO, LOGTAG, "Read:%s\n", rdbuf);
        }
}
