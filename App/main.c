/**
 * @file
 * @brief 用户程序：应用程序入口
 * @author
 * + 隐星魂 (Roy Sun) <xwos@xwos.tech>
 * @copyright
 * + Copyright © 2015 xwos.tech, All Rights Reserved.
 * > Licensed under the Apache License, Version 2.0 (the "License");
 * > you may not use this file except in compliance with the License.
 * > You may obtain a copy of the License at
 * >
 * >         http://www.apache.org/licenses/LICENSE-2.0
 * >
 * > Unless required by applicable law or agreed to in writing, software
 * > distributed under the License is distributed on an "AS IS" BASIS,
 * > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * > See the License for the specific language governing permissions and
 * > limitations under the License.
 */

#include "Board/std.h"
#include <xwos/lib/xwlog.h>
#include <xwos/osal/thd.h>
#ifdef XWCFG_LIBC__newlib
#  include <xwmd/libc/newlibac/mif.h>
#endif
#ifdef XWCFG_LIBC__picolibc
#  include <xwmd/libc/picolibcac/mif.h>
#endif
#include <xwem/vm/lua/mif.h>
#include "Board/XWAC/xwds/device.h"
#include "Board/XWAC/fatfs/sdcard.h"
#include "App/thd.h"
#include "Test/eeprom.h"

#define LOGTAG "Main"

#define MAIN_THD_PRIORITY XWOS_SKD_PRIORITY_DROP(XWOS_SKD_PRIORITY_RT_MAX, 0)

xwer_t main_task(void * arg);

const struct xwos_thd_desc main_thd_desc = {
        .attr = {
                .name = "main.thd",
                .stack = NULL,
                .stack_size = 8192,
                .stack_guard_size = XWOS_STACK_GUARD_SIZE_DEFAULT,
                .priority = MAIN_THD_PRIORITY,
                .detached = true,
                .privileged = true,
        },
        .func = main_task,
        .arg = NULL,
};
xwos_thd_d main_thd;

xwer_t xwos_main(void)
{
        xwer_t rc;

        rc = xwos_thd_create(&main_thd,
                             &main_thd_desc.attr,
                             main_thd_desc.func,
                             main_thd_desc.arg);
        if (rc < 0) {
                goto err_init_thd_create;
        }

        rc = xwos_skd_start_lc();
        if (rc < 0) {
                goto err_skd_start_lc;
        }
        return XWOK;

err_init_thd_create:
        BOARD_BUG();
err_skd_start_lc:
        BOARD_BUG();
        return rc;
}

xwer_t board_init_devices(void)
{
        xwer_t rc;

        rc = stm32xwds_uart_init();
        if (rc < 0) {
                goto err_uart_init;
        }
        /* Can print log from here */
        rc = stm32xwds_i2c_init();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Init I2C ... <rc:%d>\n", rc);
                goto err_i2c_init;
        }
        rc = stm32xwds_eeprom_init();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Init EEPROM ... <rc:%d>\n", rc);
        }
        return XWOK;

err_i2c_init:
        stm32xwds_uart_fini();
err_uart_init:
        return rc;
}

void board_fini_devices(void)
{
        xwer_t rc;

        rc = stm32xwds_eeprom_fini();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Fini EEPROM ... <rc:%d>\n", rc);
        }
        rc = stm32xwds_i2c_fini();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Fini I2C ... <rc:%d>\n", rc);
        }
        rc = stm32xwds_uart_fini();
        if (rc < 0) {
        }
}

extern void xwrust_main(void);

xwer_t main_task(void * arg)
{
        xwer_t rc;

        XWOS_UNUSED(arg);

        /* Init devices */
        rc = board_init_devices();
        if (rc < 0) {
                goto err_init_devices;
        }

        /* board_eeprom_test_write(); */
        board_eeprom_test_read();

        /* Mount sdcard */
        rc = sdcard_fatfs_mount();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Mount SDCard ... <rc:%d>\n", rc);
        }

#ifdef XWCFG_LIBC__newlib
        rc = newlibac_init();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Init newlib ... <rc:%d>", rc);
                goto err_newlibac_init;
        }
#endif

#ifdef XWCFG_LIBC__picolibc
        rc = picolibcac_init();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Init picolibc ... <rc:%d>", rc);
                goto err_picolibcac_init;
        }
#endif

        rc = child_thd_start();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Start child threads ... <rc:%d>", rc);
                goto err_child_thd_start;
        }

#if defined(XWEMCFG_vm_lua) && (1 == XWEMCFG_vm_lua)
        rc = xwlua_start();
        if (rc < 0) {
                xwlogf(ERR, LOGTAG, "Start lua VM ... <rc:%d>", rc);
                goto err_xwlua_start;
        }
#endif

        xwrust_main();

        return XWOK;

#if defined(XWEMCFG_vm_lua) && (1 == XWEMCFG_vm_lua)
err_xwlua_start:
        BOARD_BUG();
#endif
err_child_thd_start:
#ifdef XWCFG_LIBC__picolibc
        BOARD_BUG();
err_picolibcac_init:
#endif
#ifdef XWCFG_LIBC__newlib
        BOARD_BUG();
err_newlibac_init:
#endif
        BOARD_BUG();
err_init_devices:
        BOARD_BUG();
        return rc;
}
